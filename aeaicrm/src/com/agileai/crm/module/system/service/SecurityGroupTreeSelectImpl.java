package com.agileai.crm.module.system.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.crm.cxmodule.SecurityGroupTreeSelect;

public class SecurityGroupTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityGroupTreeSelect {
    public SecurityGroupTreeSelectImpl() {
        super();
    }
}
