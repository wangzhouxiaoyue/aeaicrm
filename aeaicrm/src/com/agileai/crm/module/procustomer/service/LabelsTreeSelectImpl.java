package com.agileai.crm.module.procustomer.service;

import java.util.List;

import com.agileai.crm.module.procustomer.service.LabelsTreeSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;

public class LabelsTreeSelectImpl
        extends TreeSelectServiceImpl
        implements LabelsTreeSelect {
    public LabelsTreeSelectImpl() {
        super();
    }

	@Override
	public List<DataRow> queryPerTreeRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"queryPerTreeRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
