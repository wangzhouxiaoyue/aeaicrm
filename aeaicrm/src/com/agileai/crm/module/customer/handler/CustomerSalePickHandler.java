package com.agileai.crm.module.customer.handler;

import java.util.List;

import com.agileai.crm.module.customer.service.CustomerGroupManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;

public class CustomerSalePickHandler
        extends TreeSelectHandler {
    public CustomerSalePickHandler() {
        super();
        this.serviceId = buildServiceId(CustomerGroupManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().querySalePickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "USER_ID",
                                                  "USER_NAME", "USER_SUP_ID");

        String excludeId = param.get("USER_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected CustomerGroupManage getService() {
        return (CustomerGroupManage) this.lookupService(this.getServiceId());
    }
}
