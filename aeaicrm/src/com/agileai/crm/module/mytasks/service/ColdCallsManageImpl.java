package com.agileai.crm.module.mytasks.service;

import java.util.*;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.ColdCallsManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;

public class ColdCallsManageImpl
        extends MasterSubServiceImpl
        implements ColdCallsManage {
    public ColdCallsManageImpl() {
        super();
    }

    public String[] getTableIds() {
        List<String> temp = new ArrayList<String>();

        temp.add("_base");
        temp.add("ProCustVisitInfo");

        return temp.toArray(new String[] {  });
    }

	@Override
	public DataRow getProCustRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getProCustRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
	
	public void createProVisitRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertProVisitRecord";
		param.put("PROCUST_VISIT_ID", KeyGenerator.instance().genKey());
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void updateTaskStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateTaskStateRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public DataRow getCustInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustInfoRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void updateCustStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateCustStateRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void updateTaskClassRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateTaskClassRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public DataRow getTaskReviewRecord(DataParam param) {
		String statementId = "TaskCycle8ContentManage"+"."+"getTaskReviewRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void createProCustInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertProCustInfoRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void updateTaskOrgIdRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateTaskOrgIdRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void updateTaskOrgStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateTaskOrgStateRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public DataRow getCustIdInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustIdInfoRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
		
	}
}
