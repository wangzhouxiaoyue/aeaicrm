<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>生成订单</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'createOrder'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
   <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>商机名称</th>
	<td><input name="OPP_NAME" type="text" class="text"	id="OPP_NAME"	value="<%=pageBean.inputValue("OPP_NAME")%>" readonly="readonly" size="24" style="width:302px" label="商机名称" /> 
		<input id=OPP_ID label="商机名称" name="OPP_ID" type="hidden" value="<%=pageBean.inputValue("OPP_ID")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>负责人</th>
	<td><input name="ORDER_CHIEF" type="text" class="text"	id="ORDER_CHIEF"	value="<%=pageBean.inputValue("ORDER_CHIEF")%>" size="24" style="width:302px" label="负责人" /> 
</td>
</tr>
<tr>
	<th width="100" nowrap>销售人员</th>
	<td><input name="CLUE_SALESMAN_NAME" type="text" class="text"	id="CLUE_SALESMAN_NAME"	value="<%=pageBean.inputValue("CLUE_SALESMAN_NAME")%>" size="24" style="width:302px"	readonly="readonly" label="销售人员" /> 
		<input id="CLUE_SALESMAN" label="销售人员" name="CLUE_SALESMAN" type="hidden" value="<%=pageBean.inputValue("CLUE_SALESMAN")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>订单说明</th>
	<td><textarea id="ORDER_DES" label="订单说明" name="ORDER_DES" cols="40" rows="3" style="width:540px" class="textarea"><%=pageBean.inputValue("ORDER_DES")%></textarea>
</td>
</tr>
<tr>
  <th width="100" nowrap>创建人</th>
  <td><input name="ORDER_CREATER_NAME" type="text" class="text"	id="ORDER_CREATER_NAME"	value="<%=pageBean.inputValue("ORDER_CREATER_NAME")%>" size="24" style="width:302px"	readonly="readonly" label="创建人" /> 
    <input id="ORDER_CREATER" label="创建人"  name="ORDER_CREATER" type="hidden"	value="<%=pageBean.inputValue("ORDER_CREATER")%>" size="24" class="text" />
  </td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="ORDER_CREATE_TIME" label="创建时间" name="ORDER_CREATE_TIME" type="text" value="<%=pageBean.inputTime("ORDER_CREATE_TIME")%>" size="24" style="width:302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="ORDER_STATE_TEXT" label="状态" name="ORDER_STATE_TEXT" type="text" value="<%=pageBean.selectedText("ORDER_STATE")%>" size="24" style="width:302px"  class="text" readonly="readonly"/>
	<input id="ORDER_STATE" label="状态" name="ORDER_STATE" type="hidden" value="<%=pageBean.selectedValue("ORDER_STATE")%>" />
   </td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ORDER_ID" name="ORDER_ID" value="<%=pageBean.inputValue("ORDER_ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("OPP_ID");
requiredValidator.add("ORDER_CHIEF");
requiredValidator.add("ORDER_DES");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
